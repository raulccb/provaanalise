import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import ImagePicker from 'react-native-image-picker';

export default class Autenticado extends Component {

    constructor(props) {
        super(props);
        this.state = {
            foto: null
        }
    }


    tirarFoto = () => {

        const options = {
            title: 'Tirar foto'
        };


        ImagePicker.launchCamera(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                this.setState({
                    foto: source,
                });
            }
        });

    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.boxAviso}>
                    <Text style={styles.signupTitle}>Olá, {this.props.email}</Text>
                    <Text style={styles.signupDescri}>Login Realizado com sucesso!.</Text>
                </View>
                <View style={styles.signupTextCont}>
                    <Image source={this.state.foto} />
                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.buttonText} onPress={this.tirarFoto}>Tirar Foto</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    boxAviso: {
        padding: 20
    },
    signupTextCont: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingVertical: 16,
        flexDirection: 'row',
    },
    signupText: {
        color: '#12799f',
        fontSize: 16,
    },
    signupButton: {
        color: '#12799f',
        fontSize: 16,
        fontWeight: '500',
    },
    signupTitle: {
        color: 'black',
        fontWeight: 'bold'
    },
    signupDescri: {
        color: '#ccc',
        lineHeight: 20
    }
});